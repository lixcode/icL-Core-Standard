# About standards

v.1.9 means 2019th year.

## Requests

* `?` - a new request
* `!` - an approved request
* `+` - a released request

## 22.06.2019

* `+` add `element'clickable : bool`
* `+` add `element'valid : bool`
* `+` add `element'visible : bool`
* `+` add `element.focus () : element`
* `+` add `wait:try` modifier
* `+` add `alert'present : bool`
* `+` add `wait ()` possibility
* `+` add `jammer ()` possibility
* `+` add `slot { @ }` variable description
* `+` add `Notify` chapter
* `!` add `View` chapter (to set the geometry of the page)
* `+` `handler.kill` must return a void value
* `+` fix error in chapter 26.1
* `+` fix URLs
* `+` replace `icl{}`/`icl:pro{}` by `lambda{}`/`lambda:icl{}`
* `+` add `lambda (params) : return {}`
* `+` swap chapters "Functions" and "JS Integration"
* `+` remove `any'readOnly` & `any'link`
* `+` rename `code-*` to `lambda-*`
* `+` add `Request` module
* `+` remove `Numbers`

All approved requests will be applied in icL Pro 1.9.9
