% !TeX spellcheck = en_US
\section{Web Elements}
\label{webelments}

{\bf A web \element} is a pointer to a HTML tag on the web page. \elements are a collection of pointers to HTML elements.

\subsection{Literal}

Web elements can be defined by literal \mintinline{icl}{method@target[value]}, which has the next variations:
\begin{icItems}
	\item \mintinline{icl}{css@target[CSS selector]} returns the first web element, located by CSS selector.
	\item \mintinline{icl}{css-all@target[CSS selector]} returns all web elements, located by CSS selector.
	\item \mintinline{icl}{xpath@target[XPath path expression]} returns a web element, located by XPath path expression.
	\item \mintinline{icl}{xpath@target[XPath path expression]} returns all web elements, located by XPath path expression.
	\item \mintinline{icl}{link@target[link text]} returns the link which text matches the requirements.
	\item \mintinline{icl}{link-fragment@target[link text fragments]} returns the link which text contains the required fragment.
	\item \mintinline{icl}{links-frgment@target[link text fragment]} returns all links which text contains the required fragment.
	\item \mintinline{icl}{tag-tagname@target[text]} returns a \mintinline{icl}{tagname} tag which text is \mintinline{icl}{text}.
	\item \mintinline{icl}{tags-tagname@target[text]} returns all \mintinline{icl}{tagname} tags which text is \mintinline{icl}{text}.
	\item \mintinline{icl}{input@target[name]} returns an input field which is named as \mintinline{icl}{name}.
	\item \mintinline{icl}{field@target[name]} returns an input field which has a label, named \mintinline{icl}{name}.
\end{icItems}

Where \mintinline{icl}{@target} is a value of type \mintinline{icl}{document}, \mintinline{icl}{tab}, \mintinline{icl}{window}, \mintinline{icl}{session} or \mintinline{icl}{element}.

Examples of literals —
\begin{minted}{icl}
@div = css["div"];
@a = css@div["a"];
`` @a == css["div a"]
`` @a != css["div > a"]

@selector = "#content > div:nth-of-type(3)"
css[@selector]
css["header ul > li:nth-child(even)"]
\end{minted}

\subsection{Properties}

A web element has the next properties —
\begin{icItems}
\item \mintinline{icl}{[r/o] element'attr-* : string};
\item \mintinline{icl}{[r/o] element'clickable : bool};
\item \mintinline{icl}{[r/o] element'css-* : string};
\item \mintinline{icl}{[r/o] element'enabled : bool};
\item \mintinline{icl}{[r/o] element'document : document};
\item \mintinline{icl}{[r/o] element'prop-* : any};
\item \mintinline{icl}{[r/o] element'rect : object};
\item \mintinline{icl}{[r/o] element'selected : bool};
\item \mintinline{icl}{[r/o] element'tag : string};
\item \mintinline{icl}{[r/o] element'text : string};
\item \mintinline{icl}{[r/o] element'valid : bool};
\item \mintinline{icl}{[r/o] element'visible : bool};
\item \mintinline{icl}{[r/o] elements'attr-* : list};
\item \mintinline{icl}{[r/o] elements'empty : bool};
\item \mintinline{icl}{[r/o] elements'length : int};
\item \mintinline{icl}{[r/o] elements'prop-* : list};
\item \mintinline{icl}{[r/o] elements'rects : set};
\item \mintinline{icl}{[r/o] elements'tags : list};
\item \mintinline{icl}{[r/o] elements'text : list};
\item \mintinline{icl}{[r/o] elements'(<int> n) : element};
\end{icItems}

\subsubsection{\mintinline{icl}{[r/o] element'attr-* : string}}

Returns the value of attribute \mintinline{icl}{*}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] element'clickable : bool}}

Returns \mintinline{icl}{true} if the element is clickable, otherwise \mintinline{icl}{false}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] element'css-* : string}}

Returns the value of CSS property \mintinline{icl}{*}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] element'document : document}}

Returns the document which contains this web element.

\subsubsection{\mintinline{icl}{[r/o] element'enabled : bool}}

Returns \false{} if the elements is a form element and is disabled, otherwise returns \true.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] element'prop-* : any}}

Returns the value of property \mintinline{icl}{*}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] element'rect : obj}}

Return the geometry of element on screen in CSS pixels, exactly an object with the next fields —
\begin{icItems}
	\item \mintinline{icl}{x : double} is the x coordinate relative the left edge of view.
	\item \mintinline{icl}{y : double} is the y coordinate relative to the top edge of view.
	\item \mintinline{icl}{width : double}.
	\item \mintinline{icl}{height : double}.
\end{icItems}

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] element'selected : bool}}

Returns \true{} if the element is selected, otherwise \false.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] element'tag : string}}

Return the name of tag.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] element'text : string}}

Returns the text visible on screen.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] element'valid : bool}}

Returns \mintinline{icl}{true} is the element is valid, otherwise \mintinline{icl}{false}.

\subsubsection{\mintinline{icl}{[r/o] element'visible : bool}}

Returns \mintinline{icl}{true} if the element is visible, otherwise \mintinline{icl}{false}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] elements'attr-* : list}}

Returns the value of attribute \mintinline{icl}{*} of each element. 

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] elements'empty : bool}}

Returns \true{} is the length equals zero, otherwise return \false.

\subsubsection{\mintinline{icl}{[r/o] elements'length : int}}

Return the numbers of elements in collection.

\subsubsection{\mintinline{icl}{[r/o] elements'prop-* : list}}

Returns value of property \mintinline{icl}{*} of each element converted to string.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] elements'rects : set}}

Returns position and size of each element.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] elements'tags : list}}

Returns tag name of each element.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] elements'texts : list}}

Returns the visible text of each element.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{[r/o] elements'(<int> n) : element}}

Returns the nth element.

Possible exceptions: \ferror{OutOfBounds}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsection{Operators}

There is a new operator — \mintinline{icl}{[element or elements ...] : elements}.

\subsubsection{\mintinline{icl}{[element or elements ...] : elements}}

Return a new collection which contains element from all collections.

\subsection{Methods}

{\bf The methods} are separated in 2 categories: base and additional. Base methods are defined by W3C WebDriver standard. The additional methods are defined by icL standard and can be modified in next versions of language.

\subsection{Base methods}

List of base methods —
\begin{icItems}
\item \mintinline{icl}{element.clear () : element};
\item \mintinline{icl}{element.click () : element};
\item \mintinline{icl}{element.screenshot () : string};
\item \mintinline{icl}{element.sendKeys (modifiers : int, text : string) : element};
\end{icItems}

\subsubsection{\mintinline{icl}{element.clear () : element}}

If the elements is a form element to its value will be cleared. If element is editable its property \mintinline{icl}{innerHtml} will be setted to an empty string.

Possible exceptions: \ferror{NoSessions}, \ferror{InvalidArrgument}, \ferror{NoSuchWindow}, \ferror{StaleElementReference}, \ferror{InvalidElementState} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{element.click () : element}}

Simulates a click in the middle of element and wait for navigation to complete.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{InvalidElement}, \ferror{ElementNotInteractable}, \ferror{ElementClickIntercepted} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{element.screenshot () : string}}

Returns a base64 encoded screenshot of element rectangle. You can save it as image using \mintinline{icl}{Make.image (base64 : string, path : string) : void}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{element.sendKeys (modifiers : int, text : string) : element}}

The parameter \mintinline{icl}{modifiers} get one of the following values (or a sum of some values):
\begin{icItems}
	\item \mintinline{icl}{[r/o] Key'ctrl : 1} — Control.
	\item \mintinline{icl}{[r/o] Key'shift : 2} — Shift.
	\item \mintinline{icl}{[r/o] Key'alt : 4} — Alt.
\end{icItems}

The parameter \mintinline{icl}{text} gets the text which will be typed.

Possible exceptions: \ferror{NoSessions}, \ferror{ElementNotIntractable} \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsection{Additional methods}

Additional methods may be used in testing mode using an external browser. For collection each action will be applied to each element.

\newpage
List of additional methods —
\begin{icItems}
\item \mintinline{icl}{element.child (index : int) : element};
\item \mintinline{icl}{element.closest (cssSelector : string) : element};
\item \mintinline{icl}{element.copy () : element};
\item \mintinline{icl}{element.next () : element};
\item \mintinline{icl}{element.prev () : element};
\item \mintinline{icl}{element.parent () : element};
\item \mintinline{icl}{elements.add (other : element) : elements};
\item \mintinline{icl}{elements.add (other : elements) : elements};
\item \mintinline{icl}{elements.contains (template : string) : elements};
\item \mintinline{icl}{elements.copy () : elements};
\item \mintinline{icl}{elements.filter (cssSelector : string) : elements};
\item \mintinline{icl}{elements.get (i : int) : elements};
\end{icItems}

\subsubsection{\mintinline{icl}{element.child (i : int) : element}}

Returns a new element which contains the \mintinline{icl}{i}th child element.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference}, \ferror{OutOfBounds} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{element.closest (cssSelector : string) : element}}

Returns a new element which contains the closest parent which matches CSS selector \mintinline{icl}{cssSelector}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{element.focus () : element}}

Focuses the element.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow} and \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{element.next () : element}}

Returns a new element which contains the next sibling element.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{element.prev () : element}}

Returns a new element which contains the previous sibling element.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{element.parent () : element}}

Return a new element which contains the parent element.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{elements.add (other : element) : elements}}

Add to this collection the \mintinline{icl}{other} element.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{elements.add (other : elements) : elements}}

Add to this collection all elements from collection \mintinline{icl}{other}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{elements.contains (template : string) : elements}}

Returns a new collection which contains all elements of collection which contains a fragment of text which matches the \mintinline{icl}{template}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{elements.copy () : elements}}

Returns a new collection which contains all elements of this collection.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{element.filter (cssSelector : string) : element}}

Returns a new collection which elements of this collection, which match the CSS selector \mintinline{icl}{cssSelector}.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference} (see table \ref{errors}).

\subsubsection{\mintinline{icl}{elements.get (i : int) : element}}

Returns the \mintinline{icl}{i}th element of collection.

Possible exceptions: \ferror{NoSessions}, \ferror{NoSuchWindow}, \ferror{StaleElementReference}, \ferror{OutOfBounds} (see table \ref{errors}).

\subsection{List of implicit properties}
\label{elements:predefined:properties}

In icL are implicit just properties with a data type compatible with icL —
\begin{icItems}
	\item Node:	
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-childNodes : elements};
		\item \mintinline{icl}{[r/o] element'prop-firstChild : element};
		\item \mintinline{icl}{[r/o] element'prop-innerText : string};
		\item \mintinline{icl}{[r/o] element'prop-isConnected : bool};
		\item \mintinline{icl}{[r/o] element'prop-lastChild : element};
		\item \mintinline{icl}{[r/o] element'prop-nodeName : string};
		\item \mintinline{icl}{[r/o] element'prop-nodeType : int};
		\item \mintinline{icl}{[r/o] element'prop-nodeValue : string};
		\item \mintinline{icl}{[r/o] element'prop-parentElement : element};
		\item \mintinline{icl}{[r/o] element'prop-textContent : string};
	\end{icItems}
	
	\item Element:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-className : string};
		\item \mintinline{icl}{[r/o] element'prop-clientHeight : double};
		\item \mintinline{icl}{[r/o] element'prop-clientLeft : double};
		\item \mintinline{icl}{[r/o] element'prop-clientTop : double};
		\item \mintinline{icl}{[r/o] element'prop-clientWidth : double};
		\item \mintinline{icl}{[r/o] element'prop-computedName : string};
		\item \mintinline{icl}{[r/o] element'prop-computedRole : string};
		\item \mintinline{icl}{[r/o] element'prop-id : string};
		\item \mintinline{icl}{[r/o] element'prop-innerHTML : string};
		\item \mintinline{icl}{[r/o] element'prop-localName : string};
		\item \mintinline{icl}{[r/o] element'prop-nextElementSibling : element};
		\item \mintinline{icl}{[r/o] element'prop-outerHTML : string};
		\item \mintinline{icl}{[r/o] element'prop-prefix : string};
		\item \mintinline{icl}{[r/o] element'prop-previousElementSibling : element};
		\item \mintinline{icl}{[r/o] element'prop-scrollHeight : double};
		\item \mintinline{icl}{[r/o] element'prop-scrollLeft : double};
		\item \mintinline{icl}{[r/o] element'prop-scrollTop : double};
		\item \mintinline{icl}{[r/o] element'prop-scrollWidth : double};
		\item \mintinline{icl}{[r/o] element'prop-tagName : string};
		\item \mintinline{icl}{[r/o] element'prop-baseURI : string};
	\end{icItems}
	
	\item HTMLElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-accessKey : string};
		\item \mintinline{icl}{[r/o] element'prop-accessKeyLabel : string};
		\item \mintinline{icl}{[r/o] element'prop-contentEditable : string};
		\item \mintinline{icl}{[r/o] element'prop-isContentEditable : bool};
		\item \mintinline{icl}{[r/o] element'prop-dataset : object};
		\item \mintinline{icl}{[r/o] element'prop-dir : string};
		\item \mintinline{icl}{[r/o] element'prop-draggable : bool};
		\item \mintinline{icl}{[r/o] element'prop-hidden : bool};
		\item \mintinline{icl}{[r/o] element'prop-inert : bool};
		\item \mintinline{icl}{[r/o] element'prop-lang : string};
		\item \mintinline{icl}{[r/o] element'prop-offsetHeight : double};
		\item \mintinline{icl}{[r/o] element'prop-offsetLeft : double};
		\item \mintinline{icl}{[r/o] element'prop-offsetParent : element};
		\item \mintinline{icl}{[r/o] element'prop-offsetTop : double};
		\item \mintinline{icl}{[r/o] element'prop-offsetWidth : double};
		\item \mintinline{icl}{[r/o] element'prop-spellcheck : double};
		\item \mintinline{icl}{[r/o] element'prop-title : string};
	\end{icItems}
	
	\item HTMLAnchorElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-download : string};
		\item \mintinline{icl}{[r/o] element'prop-hash : string};
		\item \mintinline{icl}{[r/o] element'prop-host : string};
		\item \mintinline{icl}{[r/o] element'prop-hostname : string};
		\item \mintinline{icl}{[r/o] element'prop-href : string};
		\item \mintinline{icl}{[r/o] element'prop-hreflang : string};
		\item \mintinline{icl}{[r/o] element'prop-media : string};
		\item \mintinline{icl}{[r/o] element'prop-password : string};
		\item \mintinline{icl}{[r/o] element'prop-origin : string};
		\item \mintinline{icl}{[r/o] element'prop-pathname : string};
		\item \mintinline{icl}{[r/o] element'prop-port : string};
		\item \mintinline{icl}{[r/o] element'prop-protocol : string};
		\item \mintinline{icl}{[r/o] element'prop-rel : string};
		\item \mintinline{icl}{[r/o] element'prop-search : string};
		\item \mintinline{icl}{[r/o] element'prop-target : string};
		\item \mintinline{icl}{[r/o] element'prop-text : string};
		\item \mintinline{icl}{[r/o] element'prop-type : string};
		\item \mintinline{icl}{[r/o] element'prop-username : string};
	\end{icItems}
	
	\item HTMLAreaElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-alt : string};
		\item \mintinline{icl}{[r/o] element'prop-coords : string};
	\end{icItems}
	
	\item HTMLButtonElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-autofocus : bool};
		\item \mintinline{icl}{[r/o] element'prop-disabled : bool};
		\item \mintinline{icl}{[r/o] element'prop-form : element};
		\item \mintinline{icl}{[r/o] element'prop-formAction : string};
		\item \mintinline{icl}{[r/o] element'prop-formEnctype : string};
		\item \mintinline{icl}{[r/o] element'prop-formMethod : string};
		\item \mintinline{icl}{[r/o] element'prop-formNoValidate : bool};
		\item \mintinline{icl}{[r/o] element'prop-formTarget : string};
		\item \mintinline{icl}{[r/o] element'prop-labels : elements};
		\item \mintinline{icl}{[r/o] element'prop-name : string};
		\item \mintinline{icl}{[r/o] element'prop-value : string};
		\item \mintinline{icl}{[r/o] element'prop-willValidate  : bool};
	\end{icItems}
	
	\item HTMLCanvasElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-height : int};
		\item \mintinline{icl}{[r/o] element'prop-width : int};
	\end{icItems}
	
	\item HTMLDataListElement: \mintinline{icl}{[r/o] element'prop-options : elements};
	
	\item HTMLFormElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-elements : elements};
		\item \mintinline{icl}{[r/o] element'prop-length : int};
		\item \mintinline{icl}{[r/o] element'prop-action : string};
		\item \mintinline{icl}{[r/o] element'prop-encoding : string};
		\item \mintinline{icl}{[r/o] element'prop-enctype : string};
		\item \mintinline{icl}{[r/o] element'prop-acceptCharset : string};
		\item \mintinline{icl}{[r/o] element'prop-autocomplete : string};
		\item \mintinline{icl}{[r/o] element'prop-noValidate : string};
	\end{icItems}
	
	\item HTMLIFrameElement: \mintinline{icl}{[r/o] element'prop-allowPaymentRequest};
	
	\item HTMLImageElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-complete : bool};
		\item \mintinline{icl}{[r/o] element'prop-crossOrigin : string};
		\item \mintinline{icl}{[r/o] element'prop-isMap : bool};
		\item \mintinline{icl}{[r/o] element'prop-naturalHeight : int};
		\item \mintinline{icl}{[r/o] element'prop-naturalWidth : int};
		\item \mintinline{icl}{[r/o] element'prop-src : string};
		\item \mintinline{icl}{[r/o] element'prop-useMap : string};
	\end{icItems}
	
	\item HTMLInputElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-accept : string};
		\item \mintinline{icl}{[r/o] element'prop-checked : bool};
		\item \mintinline{icl}{[r/o] element'prop-defaultChecked : bool};
		\item \mintinline{icl}{[r/o] element'prop-defaultValue : string};
		\item \mintinline{icl}{[r/o] element'prop-dirName : string};
		\item \mintinline{icl}{[r/o] element'prop-indeterminate : bool};
		\item \mintinline{icl}{[r/o] element'prop-list : element};
		\item \mintinline{icl}{[r/o] element'prop-min : string};
		\item \mintinline{icl}{[r/o] element'prop-max : string};
		\item \mintinline{icl}{[r/o] element'prop-maxLength : int};
		\item \mintinline{icl}{[r/o] element'prop-multiple : bool};
		\item \mintinline{icl}{[r/o] element'prop-pattern : string};
		\item \mintinline{icl}{[r/o] element'prop-placeholder : string};
		\item \mintinline{icl}{[r/o] element'prop-readOnly : bool};
		\item \mintinline{icl}{[r/o] element'prop-required : bool};
		\item \mintinline{icl}{[r/o] element'prop-selectionStart : int};
		\item \mintinline{icl}{[r/o] element'prop-selectionEnd : int};
		\item \mintinline{icl}{[r/o] element'prop-selectionDirection : string};
		\item \mintinline{icl}{[r/o] element'prop-size : int};
		\item \mintinline{icl}{[r/o] element'prop-step : string};
		\item \mintinline{icl}{[r/o] element'prop-validity : bool};
		\item \mintinline{icl}{[r/o] element'prop-validationMessage : string};
		\item \mintinline{icl}{[r/o] element'prop-valueAsNumber : double};
	\end{icItems}
	
	\item HTMLLabelElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-control : element};
		\item \mintinline{icl}{[r/o] element'prop-htmlFor : string};
	\end{icItems}
	
	\item HTMLLinkElement: \mintinline{icl}{[r/o] element'prop-as : string};
	\item HTMLMapElement: \mintinline{icl}{[r/o] element'prop-areas : elements};
	
	\item HTMLMediaElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-autoplay : bool};
		\item \mintinline{icl}{[r/o] element'prop-controls : bool};
		\item \mintinline{icl}{[r/o] element'prop-currentSrc : string};
		\item \mintinline{icl}{[r/o] element'prop-currentTime : double};
		\item \mintinline{icl}{[r/o] element'prop-defaultMuted : bool};
		\item \mintinline{icl}{[r/o] element'prop-defaultPlaybackRate : bool};
		\item \mintinline{icl}{[r/o] element'prop-disableRemotePlayback : bool};
		\item \mintinline{icl}{[r/o] element'prop-duration : double};
		\item \mintinline{icl}{[r/o] element'prop-ended : bool};
		\item \mintinline{icl}{[r/o] element'prop-loop : bool};
		\item \mintinline{icl}{[r/o] element'prop-mediaGroup : string};
		\item \mintinline{icl}{[r/o] element'prop-muted : bool};
		\item \mintinline{icl}{[r/o] element'prop-networkState : int};
		\item \mintinline{icl}{[r/o] element'prop-paused : bool};
		\item \mintinline{icl}{[r/o] element'prop-playbackRate : double};
		\item \mintinline{icl}{[r/o] element'prop-preload : string};
		\item \mintinline{icl}{[r/o] element'prop-readyState : int};
		\item \mintinline{icl}{[r/o] element'prop-seeking : bool};
		\item \mintinline{icl}{[r/o] element'prop-volume : double};
	\end{icItems}
	
	\item HTMLMetaElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-content : string};
		\item \mintinline{icl}{[r/o] element'prop-httpEquiv : string};
	\end{icItems}
	
	\item HTMLMeterElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-high : double};
		\item \mintinline{icl}{[r/o] element'prop-low : double};
	\end{icItems}
	
	\item HTMLModElement: \mintinline{icl}{[r/o] element'prop-cite : string};
	
	\item HTMLOListElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-reversed : bool};
		\item \mintinline{icl}{[r/o] element'prop-start : int};
	\end{icItems}
	
	\item HTMLOptionElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-defaultSelected : bool};
		\item \mintinline{icl}{[r/o] element'prop-index : int};
		\item \mintinline{icl}{[r/o] element'prop-label : string};
		\item \mintinline{icl}{[r/o] element'prop-selected : bool};
	\end{icItems}
	
	\item HTMLProgressElement: \mintinline{icl}{[r/o] element'prop-position : double};
	
	\item HTMLScriptElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-charset : string};
		\item \mintinline{icl}{[r/o] element'prop-async : bool};
		\item \mintinline{icl}{[r/o] element'prop-defer : bool};
		\item \mintinline{icl}{[r/o] element'prop-noModule : bool};
	\end{icItems}
	
	\item HTMLSelectElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-selectedIndex : int};
		\item \mintinline{icl}{[r/o] element'prop-selectedOptions : elements};
	\end{icItems}
	
	\item HTMLTableCellElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-abbr : string};
		\item \mintinline{icl}{[r/o] element'prop-cellIndex : int};
		\item \mintinline{icl}{[r/o] element'prop-colSpan : int};
		\item \mintinline{icl}{[r/o] element'prop-rowSpan : int};
		\item \mintinline{icl}{[r/o] element'prop-scope : string};
	\end{icItems}
	
	\item HTMLTableColElement: \mintinline{icl}{[r/o] element'prop-span : int};
	
	\item HTMLTableElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-caption : element};
		\item \mintinline{icl}{[r/o] element'prop-tBodies : elements};
		\item \mintinline{icl}{[r/o] element'prop-tHead : element};
		\item \mintinline{icl}{[r/o] element'prop-tFoot : element};
	\end{icItems}
	
	\item HTMLTableRowElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-cells : elements};
		\item \mintinline{icl}{[r/o] element'prop-rowIndex : int};
	\end{icItems}
	
	\item HTMLTextAreaElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-cols : int};
		\item \mintinline{icl}{[r/o] element'prop-textLength : int};
		\item \mintinline{icl}{[r/o] element'prop-wrap : string};
	\end{icItems}
	
	\item HTMLTimeElement: \mintinline{icl}{[r/o] element'prop-dateTime : string};
	
	\item HTMLTrackElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-kind : string};
		\item \mintinline{icl}{[r/o] element'prop-srclang : string};
		\item \mintinline{icl}{[r/o] element'prop-label : string};
		\item \mintinline{icl}{[r/o] element'prop-default : bool};
	\end{icItems}
	
	\item HTMLVideoElement:
	\begin{icItems}
		\item \mintinline{icl}{[r/o] element'prop-poster : string};
		\item \mintinline{icl}{[r/o] element'prop-videoHeight : int};
		\item \mintinline{icl}{[r/o] element'prop-videoWidth : int};
	\end{icItems}
	
	% \item \mintinline{icl}{[r/w] element'prop-};
\end{icItems}

The \mintinline{icl}{rows} is absent in this list, because for different element it may have different type.
