@time = 30000; `` 30 seconds

$func = (@name : string) {
    Log.info "It's working, " + name + "!";
};

js{
	window.setTimeout(() => {
		${func}(crossfire.string("Andrew"));
	}, arguments[0])
}
.crossfire()
.run(@time);
